u.prototype.toggleClass = function (cls) {
    if (u(this).hasClass(cls)) {
        u(this).removeClass(cls);
    } else {
        u(this).addClass(cls);
    }
}

u(".navbar-burger").handle("click", function() {
    // Check for click events on the navbar burger icon

    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
    u(".navbar-burger").toggleClass("is-active");
    u(".navbar-menu").toggleClass("is-active");

});

u(".notification .delete").handle("click", function() {
    u(this).closest(".notification").remove();
});

let headings = u("h2, h3, h4, h5, h6");
headings = headings.filter(function (node) {return !u(node).hasClass("subtitle")});

for (heading of headings.nodes) {
    let id = u(heading).attr("id");
    let link = document.createElement("a");
    link.innerHTML = "🔗";
    let href = document.createAttribute("href");
    href.value = `#${id}`;
    let cls = document.createAttribute("class");
    cls.value = "hyperlink";
    link.setAttributeNode(href);
    link.setAttributeNode(cls);
    heading.append(link);
}