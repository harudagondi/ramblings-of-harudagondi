+++
title = "about me"
+++

My name is Gio or Lux. You can call me with either of them.

I write poetry, sometimes prose, sometimes screenplays. I would love to make a novel of my own, but I suck at writing sceneries. I have a bunch of social media accounts just because I don't anyone to take my username.

## username

To be honest, there are no real reasons that i picked `harudagondi` as my username. `haruda` isn't even a Japanese name. The first part of the username is the first one I've used for my Roblox account when I was eight years old, probably because: I watch anime since I was four, and; I like it. My username isn't my pseudonym per se, nor I do not want people to refer to me as `harudagondi`. Just don't. Call me Lux or Gio instead.

The `gondi` part of my username refers to *Toxoplasma gondii*. When I first heard about this parasite, I was interested with its symptoms. According to [livescience.com](https://www.livescience.com/56529-strange-facts-about-toxoplasma-gondii-parasite.html), it is referred to as the "Mind-Control" parasite, and it is not hard to see why. In summary, there are three main animals it infects (but it can leap on almost all warm-blooded animals): rats, cats, and humans. First, infected rats lose their fear of cats, and gain a type of "sexual attraction" to cat urine's smell. These rats are drawn towards cats, thus infecting them. Since a lot of cats live with their human owners, these people can also become infected.

*T. gondii* has mild or no symptoms on most typical humans. However, it could cause mental illnesses such as schizophrenia, bipolar disorder, and others. Because of its lack of symptoms, it is possible that half of the human population are infected. Lastly, there is **no cure** for this parasite.

I think that is very interesting, so I appended it to my original username.

## SOGIE Identity

Since I am still young, I am still exploring my identity and sexuality. This section is bound to change, so check this article again from time to time.

For my sexual orientation, I found this venn diagram from Twitter.

![A venn diagram of bisexuality, polysexuality, pansexuality, and omnisexuality](sexuality-venn-diagram.jpg)

According to this diagram, the sexuality that describes me the best is omnisexuality. I mostly find both masculine (but) soft men and feminine (but) dom women hot. Now that I think about it, the vice-versa could be true; masculine submissive women and feminine dominant men are also hot. Anyways, in my sexuality, I emphasize the masculinity/femininity of a person, and I find different personalities attractive based on their gender expression. However, I don't really care what gender of a person is, as long as I have the hots for him, it's fine.

I don't know if I'm a little bit aromantic. Might have to explore that later.

My gender is a clusterfuck.

For context, let me describe you my adolescence. First of all, all of the people I know told me that the pitch of my voice is very high. I liked it. But as I hit puberty, my voice started getting deeper, which triggered my gender dysphoria. I don't want to be a manly man (but I do wanna get ripped), and I prefer being feminine. *But I don't want to be a woman*.

I like the idea of being genderfluid and genderflux. Sometimes I don't really care what my gender is, but other times my physicality cause too much gender dysphoria, whether I want to be feminine or masculine.

For my physicality, I want my voice to be androgynous, at least. For my body, I wanna be thin but muscular (think: twunks).

Anyways, that's my SOGIE. My pronouns are [he/she/they/fae](https://pronouny.xyz/u/harudagondi).
