+++
title = "social media accounts"
+++

twitter: [@harudagondi](https://www.twitter.com/harudagondi) / [@rakusapolonomio](https://www.twitter.com/rakusapolonomio)  
instagram: [@harudagondi](https://www.instagram.com/harudagondi)  
mastodon: [harudagondi@mastodon.social](https://mastodon.social/@harudagondi)  
friendica: [harudagondi@venera.social](https://venera.social/profile/harudagondi/profile)  
github: [@harudagondi](https://github.com/harudagondi)  
gitlab: [@harudagondi](https://gitlab.com/harudagondi)  
reddit: [u/haruda_gondi](https://reddit.com/u/harudagondi)  
ko-fi: [harudagondi](https://ko-fi.com/harudagondi)  
liberapay: [harudagondi](https://liberapay.com/harudagondi/)  
letterboxd: [harudagondi](https://letterboxd.com/harudagondi/)  
telegram: [@harudagondi](https://t.me/harudagondi)  
signal: message me on telegram  
session: message me on telegram  
protonmail: [harudagondi](harudagondi@protonmail.com)  
tutanota: [harudagondi](harudagondi@tutanota.com)  
