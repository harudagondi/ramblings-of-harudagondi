+++
title = "commissions"
+++

## Terms of Service (TOS)

By filling up this form, you accept and agree to be bound by the terms and provision of this agreement, which may be posted and modified from time to time. All such guidelines or rules are hereby incorporated by reference into the Terms of Service.

SUBMITTING THIS FORM WILL CONSTITUTE ACCEPTANCE OF THIS AGREEMENT. IF YOU DO NOT AGREE TO ABIDE BY THE FOLLOWING TERMS, PLEASE DO NOT SUBMIT THIS FORM.

## Payment

- I accept payments through Paypal, GCash, or Paymaya.
- I will only start a commission if you pay me at least 50% of the price upfront. 100% upfront is nice, but you don't have to.
- I will only give you the end product if you paid the remaining amount.
- If you live in the Philippines, please pay me using PHP. Otherwise, please pay me using USD on Paypal.

## Process

- I do not send WIPs (Work-In-Progress).
- Small revisions are only allowed (Grammatical Issues and minimal word choices).
- Large revisions will incur an additional fee of at least ₱100/$2, depending on the revisions itself.

## Deadline, Waiting Times, and Refunds

- I don't accept deadlines.
- For 1-stanza poems and short poems, if I don't get back to you in one week, I will refund your payment.
- For long poems, if I don't get back to you in one month, I will refund your payment.
- I will not refund the initial payment if you, for some reason, do not wish to pay the remaining amount.

## Types of Commission

The poems can be rhyming or free-verse. I do not write sonnets, poems with strict rhythm, etc.

### 1-Stanza Poem

- If you live in the Philippines, the base cost is ₱100.
- If you live outside the Philippines, the base cost is around $5.
- The length of the poem will be around 3-7 lines, less than 100 words.

### Short Poem

- If you live in the Philippines, the base cost is ₱350.
- If you live outside the Philippines, the base cost is around $20.
- The length of the poem is around 3-5 stanzas, 3-5 lines each, around 200 words or more.

### Long Poem

- If you live in the Philippines, the base cost is ₱1000.
- If you live outside the Philippines, the base cost is around $50.
- The length of the poem is around 1000 words or more.

### Custom

- Your custom commission depends on how long I make it.
- Estimation of price is around ₱400 per hour in the Philippines, or $20 per hour outside the Philippines.

## Do's and Dont's

I usually write:

- Melancholia
- Surreal
- Abstract
- Themes about death, ghosts, etc.

I can write:

- Romance
- Implicit Sexual Themes (You must be at least 18 years of age!)
- Soft Horror
- Creepy Themes

I do not write:

- Explicit Sexual Themes
- NSFL (Not Safe For Life) Topics
- Poems that are for completing school requirements
- Hateful and Bigoted Themes

## Extra Information

- I have the right to refuse a commission without explanation.
- All commissions are licensed for personal use only. Specifically, all works will be licensed under CC BY-NC-SA 4.0. To view a copy of this license, visit [https://creativecommons.org/licenses/by-nc-sa/4.0](https://creativecommons.org/licenses/by-nc-sa/4.0)
- If you wish to use the poem for commercial purposes, please pay 1000% the original price. All works made for commercial purposes will be licensed under CC BY-SA 4.0. To view a copy of this license, visit [https://creativecommons.org/licenses/by-sa/4.0](https://creativecommons.org/licenses/by-sa/4.0)

## Form

Submit a form [here](https://forms.gle/nKrYLjiEdYeLqiVQ9).
