+++
title = "undivine"
[extra]
has_comments = true
has_donate_button = true
+++

## yeah,

### can't believe thats all over

```
when five months pass
and we sleep; why even bother?

it's too much
too fast

when's dessert?
five past two
in the morning.

i remember the date
of the mourning.

he burst down to flowers,
she cried, scorning
the way he left the world
undeserving



i secretly stopped



i already knew
he was dead
from the moment
i was alive

the angel whispered
at sixteen past ten months

the ghost that haunts
secrets of the lamps
night of the camp

the dream
screamed


i asked him to put down the shoes
he told me to put down the world


he already knew
how the dreams scatter

and blew
no matter

how true
the shudder

of doors
closed

the windows
divine

boast
and remind


how we used to escape from the worlds.




divinity
by porter robinson

chop the vocals
feel the rhyme
as the tender locals
say shit online


hey
it's kkb
come with me
say hello
to the future


where i, for once
will nurture
the pain and gift
of loving
you,




but wait!





everyone hates me,

so love is meaningless

even if they don't

they'll know, anyway.



they'll know

the secrets
that i keep

hidden away
giving weight



because



i hate myself

i hate everyone
with all my passion


lost in the inevitable


too bad
i can't feel


the redeemable
that i'm not


laughable
that everyone else (know)


deplorable
that i'll always be


unforgettable
that what's you are


too bad



i'll always forget
what had happened
in 19XX
where you killed me
in fresh blood

while i computed the lexicon
ten thousand words
in your name

in 60 frames per second,
you'll last a sixth
of a blink of the eye

just like it goes too fast
rolling, rolling
until  the camera stops


audience leave


actors rest


i stand here in despair


the stage of stories unknown



no one wanna watch
my movie
my life
my problems
my cry


because

no


one



will




care





anyway.







no








one









will.
```
