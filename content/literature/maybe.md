+++
title = "maybe;"
[extra]
has_comments = true
has_donate_button = true
+++

<pre>
<em>maybe i'll return.</em>

dancing in the middle,
death day, it's a riddle.

do you have to make me dance
to the rhythm
the beat; the screech
bass drop, i fall

flickering lights
moths of the mouths
i bleed of eyes
tears of scars

did you remember when
you forced me to go?

well,
i'm going back.

i'm going to find
myself
in the middle of yesterday
as i left it
on the past of tomorrow.

it has been six months.
did you still remember my name?

or did i forgot myself,
in the town over yonder;
as the dreams fall and topple
and the rain of flowers singing
in your name.

maybe i'm going back to tanauan.
i'll say hello to myself,
as i left a snapshot of yesterday
to be given tomorrow.

or maybe
i won't
say
hello
to
you.

liar.

i'll bring you back to the present.

and maybe i'll thank myself

for bringing back the old self,

before i drowned on the apathy of never.

and maybe,

i'll restart the cycle again.

it's your fault,

anyway.
</pre>