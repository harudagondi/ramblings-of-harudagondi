+++
title = "traveler on the river"
[extra]
has_comments = true
has_donate_button = true
+++

<pre style="text-align: center;">
"Good morning, Mr. Traveler!" said the River
as She gracefully descend the mountain;
the Traveler, cold as winter, gave no shiver
while crossing the path to the eternal fountain.

"Tell me, where are you going?" asked the River
whose curiosity knows no bounds
and His mouth knows no way to deliver
the words She coveted of sounds.

She asked the same question,
same day, never-ending;
waiting for the digression,
He continue on the bridge, unfaltering.

Every single day, the Traveler crosses
the bridge, but never came back.
Every single night, the River guesses
whether or not
will He consider
the River
His
fallback.

"I wonder…"
River ponders,
"if He knew
how much
I know His hue."

"I am not alone."
She stated,
seeing the numerous
men walk above
Her abode,
listening, She behoves;
words that goads
the
River
Herself.

Poetry is His Passion,
His short-lived ration.

Abstraction was His Majesty,
wretched form of tragedy.

Pace is His Skill,
running from His kill.

He was once a leader,
hubris being His bleeder.

Reading the Stories that they haul
Living on dreamscapes, His Downfall.

She sees the countless
people crossing
coming back
and forth
a lack
of fort

secrets
unravel
gossips
travel

of the Man
walking
a one-way plan
never returning

Each, and every day
every minute
astute ray
looking every minutiae

of the Traveler
on the River.

Alive and well,
it seems that
he is not
a robotical fantasy,
but a multicolored actuality.

Murmurs trickle,
He loves to tinker.
Daydreams linger,
as He stares at the
inevitable chasm
hearing the bellows
of the raging
River.

She…
doesn't know
what to say.

His eyes continued on
the water flowing,
antithetical whereupon
to the Soul within.

The thoughts whispered,
the flow reigned supreme.
The voice crackled,
words were made into the stream.

As the everyday broke, the River asked:
"Why do you cross the bridge unfailingly?"

The Traveler stops.

He looks down on the water,
flowing as if rushing
not too fast, grace of the River;
curiosity impacts, air clearing.

The Traveler met the River,
and the River made a friend.

Every day, She asked questions.
Every night, He left answers.

"It seems that this Traveler
is more than what he seems.
Yet He never confer,
only leaving traces of notes and dreams."

"A composer and a poet, He
would say: writing music,
threading words for free,
no restriction, so fluid."

"Creating works of art,
tinged of derelict despair.
He calls out His Heart;
Only a hole was found, not a spare."

"And it seems that the
people love His prowess
on Algebraic Construction. Although a
flash of annoyance was boundless."

"So He escapes to his World,
full of magick and mystery.
Dreamscapes scattered and swirled;
unbounded by any consistory."

It was night,
and in spite
of everything,
She didn't hear
a single syllable
from the Man who left
them; who is love-bereft.

Not a lot was loved of Him,
but also not much was grim.

See, He's a helper,
applying His talents
as people said: 'He's clever.'
and so, He hated that.

His Digital Aptitude;
His Muse of Scripts;
His Mouth of such Shrewd;
Critical Thinking equipped;

He hated all of that.

Sooner, he stayed on the bridge,

thinking.

sleeping.

dreaming.

I saw his Dreams.
I saw the fountain.
Demons schemes,
Sins counted.

His mind was unbreakable.

Because there was nothing to be broken.

Blank Canvas.

Painted with Fury.

Gloomy Stanzas.

Written and Buried.

I see Him.

Sleeping in His Dreams.

He's…
angry about
something.

He hates
His talents
because
He felt
used,
degraded.

He feels that
nobody cares.

A silent wanderer.
Follower of none.

He is alone,
in a room full of people,
She hears His heartbeat,
rhythmless,
meaningless.

He hates His greatest assets.
He wants the social aspect.
He hates how His friends felt fake;
Them being made out of plastic snakes.

He hates Himself.

He don't have any close friends;
He don't know how to make them.

He hates that He can't relate to them.
He hates the fact that
He is
not like
the others.

An anomaly of society.
A lonely fish of the sea.
Complete with full-on anxiety.
He's scared; He'd wanted to flee.

Flee.

Run.

Far away.

From everything he had built.

He's scared of the future.
Difficult to predict.
Wounds of past must be sutured.
Careful not to inflict

damage onto Himself.

…

Everything is difficult,
everything is weird.
Social rules are an occult,
just as everything he feared.

His actions,
Their words;
His redactions,
Nobody heard.

Nobody.

wants.

to.

hear.

him.

scream.

He fell.
She didn't notice.

He's asleep.
She's trapped in His dreams.
Bursting at the seams.
She wanted to save Him.

He is below the chasm.
No enthusiasm.
Empty.
Void.

In his dreams, He wanted to change.
In the River, His mind is deranged.

He hated how He hated Himself.

He hated how He wants to be unique,
yet He wanted to make friends.

Nonconformity is a social suicide.
"Oh, you think you're so special?"
they always snide.
His flowers rotting of petals.

Of course, He knows that
that thought isn't real.
They aren't real.
She isn't real.
He isn't real.

A side effect of dreamscapes.
Delusional forms of reality.
Every illusion takes all shapes.
He's dreaming about this poem, isn't He?

…

Well…

She wanted to tell you:

"Does this have to be fake?
Does this have to stay
in your mind, causing heartaches?"
Do you have to be afraid?"

…

He's still asleep,
flowing on the River;
but His arm reach for the steep
cliff, climbing without a shiver.

"This doesn't have to end like this."
He realized.

So He realized the dream.
And made it real.

"Guess not." said the River.
As She flows to the end.

Unfaltering.
</pre>