+++
title = "umaga, hapon, gabi"
[extra]
has_comments = true
has_donate_button = true
+++

**ala-seis ng umaga** 

Gumising ako.

**ala-siete ng umaga**

Kumain ako ng almusal.

Habang ikaw ay nasa isip ko.

**alas-ocho ng umaga**

Nag-ayos ako ng gamit.

Sumakay ako ng jeep.

Habang ikaw ay nasa isip ko.

**alas-nueve ng umaga**

Nasa paaralan na ako.

Hinihintay ko pa rin ang tawag mo.

Hinihintay ko pa rin ang ngiti mo.

Hinihintay ko pa rin ang puso mo.

**alas-diyes ng umaga**

Nakita kita sa ilalim ng kabalyero.

May hawak kang papel, di ko alam kung para saan.

Lumapit ako, pero nakangiti ka pa rin sa iyong mundo.

Ako pa ba? O siya na?

Lumayo na lang ako.

**alas-onse ng umaga**

Naghihintay ako sa ilalim ng akasya.

Sabi mo sa akin, nandito ka na ng alas-onse imedya.

Kaya umupo ako at tumayo.

Lumakad ako at tumakbo.

Tumulog ako at gumising.

Habang hinahanap ko ang mukhang di ko kilala.

**alas-dose ng hapon**

Nandito lang ako sa silid-aralan.

Pag-aralan mo nga kung ano ba tayo.

Binigyan mo ako ng rosas, ngunit walang tinik.

Binigyan mo ako ng apoy, ngunit walang init.

Binigyan mo ako ng kalihim, walang lipstick.

Binigay mo na ang lahat ng perpekto, sapagkat wala ako.

Sapagkat minahal mo ako, dangal ako ni Kupido?

**ala-una ng hapon**

Kumakain ako sa canteen, mag-isa.

Nandito ka sa tabi ko, pero nasaan ka?

Nakikita ko ang mukha mo, pero nasaan ang iyong mata?

Nakangiti ka, pero bakit ang iyong bibig ay nawawala?

Nandito ako sa canteen at wala akong kasama.

Nawawala ako at nawawala ka.

Kaya nagwala ka: sabi mo, "Nawawala na siya!"

Habang ako'y nandito, nagwawala na lang.

**alas-dos ng hapon**

Kahit huli ka na, nandito pa rin ako.

Hinihintay ko ang tadhanang sinasabi mo sa akin.

Sa ilalim ng akasya, nakahiga ako.

Apat na pu't pitong beses ako nagtext sa number mo.

Tinatawagan kita ng mahigit tatlong oras.

Gusto ko itanong kung saan ba talaga tayo magkikita.

Sa ilalim ba ng akasya, o sa tawag ng puso mo?

Wala ka atang load, bakit ayaw mo akong pansinin?

Kahit emosyon ko lang ang iyong damdamin?

**alas-tres ng hapon**

Umuwi ako.

Naka-dalawang daan, limampu't isa ang text ko sa iyo.

Bakit ayaw mo sumagot?

Ang pinakahuli mong sinabi ay:

"Tagpo tayo sa ilalim ng akasya."

"Gusto ko na makita ang mukha mo."

"Gusto ko marinig ang boses mo."

"Ikaw ang nagustuhan ko."

Kaya hinanda ko ang sarili.

Para sa pagkakataong mapapunta ako sa iyong piling.

**alas-kwatro ng hapon**

Umuwi ako.

Kinain ko ang kalihim, sana di kasing kapula ng iyong puso.

Nasa akin na ang regalong binili mo sa SM.

Nakabalot siya sa papel mo sa buhay.

Sa papel na hindi nakasulat ang pangalan ko.

Binigay mo sa akin ang pusong mapula.

Tumitibok pa.

Pero para sa akin kaya ito?

Nalalanta na.

Ito ba ang pagmamahal mo?

Ayoko na.

**ala-singko ng hapon**

Umuwi ako.

Dinaya mo ako.

Nakita kita sa ilalim ng kabalyero.

Nalulunod ka sa pulang bulaklak.

Ang saya mo sa sinisinghot mong droga.

Hindi na kita kilala, dahil wala ka nang tinira.

Tara mawalan tayo ng malay sa iyong pagmamahal.

Sa pagkagising ko ay ika'y sa aking karibal.

Iniwanan mo ako sa ilalim ng kabalyero.

Nalulunod sa dagat ng bulaklak.

Pababa ako ng pababa.

Hanggang mabaon ako sa lupa.

**ala-seis ng gabi**

BAKIT AYAW MO AKO PANSININ!

Hinintay kita ng pitong oras hindi ka man lang nagpakita.

Tinext kita ng pitong daan, anim na pu't apat na beses,

hindi ka pa rin sumasagot!

Umasa ako na dadating ka,

Wala talaga!

Paano ko makikilala ang mukhang hindi nagpapakita?

Paano ko madadama ang kamay mong kalamig?

Paano mo mararamdaman ang pagmamahal kong tunay?

Kung wala ka?

Kung wala ako?

Kung walang tayo?

Tayo ba talaga?

**ala-siete ng gabi**

Sabihin mo nga sa akin!

Ano ba tayo?

Tayo ba talaga?

Sa tingin mo ba naaliw ako sa rosas mong walang tinik?

Sa tingin mo ba masarap ang kalihim kong kinain?

Sa tingin mo ba nagustuhan ko ang puso mo?

Bakit mo binibigay itong lahat?

Wala namang halaga.

Binigay mo na ba ang lahat?

Yun lang ba talaga?

Mahal mo ba ako?

Bakit wala akong nararamdaman?

**alas-ocho ng gabi**

Ganto ba ako magmahal para sa'yo?

Ganto ba ang gagawin ko para sa'yo?

O ganto ba ako katanga para sa'yo?

Ayoko na, ayoko na, ayoko na.

Hinarot mo ako.

Minahal mo ako,

Linayuan mo lang ako.

Sana naman hindi mo ako hinayaan malunod sa dagat mo.

Sa libo-libong bulaklak na binigay mo sakin, hinayaan ko lang mabulok.

Dahil gusto ko isama sa aking kabaong,

ang dating ako na patay na patay para sa iyo.

**alas-nueve ng gabi**

Ako ba ay bulag?

Hindi kita makita.

Ako ba ay bingi?

Wala ako marinig.

Patay ka na ba?

Kalamig mo na.

Nawawala ang esensya ng lahat ng bagay.

Kung walang tamis ang iyong pinagpugay.

Sana naman ika’y tumingin sa langit,

At makikita mo ang talang nagniningning.

**alas-diyes ng gabi**

Bakit ba ako’y nasanay?

Akala ko nandito ka sa tabi ko, pero wala ka!

Nawawala ang mata ko, nasaan ka!

Hindi kita makita dahil sa mata kong lumuluha,

Sa totoo lang, ayaw ko na makita,

Ang taong tunay kong nakilala,

Taong may butas sa kanyang dibdib.

Dinibdib ko siya, pero di ko kaya.

Kaya hinayaan ko na lang.

**alas-onse ng gabi**

Isang kalahating araw ang lumipas.

Naka-anim na libo, dalawampu’t tatlo ang text ko sa iyo.

Labing-dalawang oras ang hinintay ko.

Hindi ako makatulog dahil mo!

Ayaw ko matulog dahil sa’yo!

Pansinin mo ako, pansinin mo ako, kahit isang beses lang.

Lambingin mo ako, kahit isang minuto lang, magiging masaya na ako.

PERO AYAW MO ATA SA AKIN EH! HINDI MO AKO MAHAL!

**alas-dose ng madaling-araw**

*bakit gising ka pa?*

Bakit gising pa ako?

Di ko alam kung papansinin mo pa ako.

Di ko alam kung tunay ka.

Tao ka ba, o tanga ako?

Akala ko tao ka.

Yun pala hindi.

Inuto mo lang ako. 

**ala-una ng madaling-araw**

Bakit ikaw pa rin ang nasa isip ko?

Bakit bumibisita ako sa sementaryo?

Kung saan namatay ang iyong pag-ibig.

Ibinaon ko na sa lupang ilalim.

Sapagkat gusto na kita kalimutan.

Habang ako’y nasa dagat ng bulaklak.

**alas-dos ng madaling-araw**

Nakikita mo na ba ang mga bulalakaw?

Kung paano sila bumagsak?

Alam mo ba, parang sila’y araw?

Ang liwanag nila.

Paano ka?

**alas-tres ng madaling-araw**

Ilang dekada na ako nandito.

Ngayon lang ako natutong lumangoy.

Pinakawalan ko na ang pagmamahal mo.

Ginamit ko na lang ang pagmamahal ko.

**alas-kwatro ng madaling-araw**

Nakahiga ako sa gitna ng mundo.

Hinahanap ko ang sentro ng mga tala.

Iniikutan ng lahat ng puso.

**ala-singko ng madaling-araw**

Lumiliwanag na ang langit,

Habang sumisikat ang araw.

**ala-seis ng umaga**

Gumising ako.