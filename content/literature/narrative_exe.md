+++
title = "narrative.exe"
[extra]
has_comments = true
has_donate_button = true
+++

**Meteor Showers**  
*"a comet is arriving around 10:37 at night;  
and citizens are not advised to go out with lights,  
and even though they will try with their might,  
the starry sky will defiantly shine bright."*  
  
**Bilogo**  
"Manong, bayad po.  
tatlo, estudyante'y dalawa.  
Bababa sa Bilogo,  
Makikiramay sa multong nawawala."  
  
**Problem Set #2**  
On the jeep, I was answering  
a Problem-Set, the second of which  
I heard a loud scream, saying "STOP!"  
My pen slipped my fingers, and stabbed another.  
  
**Stars**  
have you seen the stars in the sky?  
the beautiful constellations, flying by?  
do you wanna sleep and dream happily?  
or do you wanna wake up, and try to look at me?  
  
**Rainbow**  
Everyone is happy and merry as they can be;  
Flocking with the wind, talking to me,  
Their voices echoing with the screams;  
Wondering if they could love with glee.  
  
**Programmable**  
a junk of mathematical equations.  
she'd spit out, adding up the notations.  
we're in the matrix, undoing the rotations;  
deliberately calculating the quaternions  
needed to reverse the reverberations.  
whew, compute the mechanization  
of the thousand echolocations.  
"be quiet, my robotical fascination."  
  
**[tell the story]**  
spare no detail  
the soul shall be known  
for she will not hail  
the name of king dethroned.  
  
**cat's death**  
she'd look upon the house  
of which she discovered the mouse  
lying with the cat, who's now dead,  
strung around with the bell of lead.  
  
**Asleep**  
he fell into a deep slumber,  
waiting for the world divine.  
i wonder if he'll remember  
to wake up from his dreams on time.  
  
**Hospital**  
diapers, tissue, and some cotton balls.  
prick the finger to know  
the blood chemistry, uh oh,  
diagnostics said you will mourn.  
  
**suture**  
i open my third eye.  
i saw the ghosts of the future  
swirl through the heart of my  
dreamscapes that, in time, will rupture.  
  
**day of the school**  
today is the day  
where i can go  
and learn  
nothing  
at all  
where are my classmates?  
have they gone home?  
where are the teachers?  
are they sleeping on the floors?  
all i see is the dull red ink  
on the countless test papers  
lying around, with the debris  
falling, falling, falling  
until it hit a head of mine  
as i fell asleep.  
goodbye, everyone!  
class dismissed. forever.  
  
**trees**  
sense the spirit of the trees  
as we dance around the graves  
of our ancestors; grieve the breeze  
that circle the flowerbeds of the brave.  
  
**august 24**  
three soldiers were filed with cases of rape  
against a fourteen-year-old daughter  
of the Manobo from Talaingod; her mouth agape  
when she discovered the cases were dropped  
after they pay P63000 and deny the blotter.  
  
**september 2**  
when i got home from school,  
ten houses and an institute burned to ash;  
i discovered the cruel  
soldiers laughing; while i furrow my brow and gnash.  
  
**denial**  
please lie to me, babe.  
as i do not want the world  
to tell me the truth.  
  
**red room**  
in the living room,  
i saw both of my parents dead.  
as the strings of fate loomed,  
it seems that the world hates us again.  
  
**3959**  
erasing myself from the narrative.  
i look through the glass, in pain.  
the conceptual being sees a declarative  
and ate through the meaning of the name.  