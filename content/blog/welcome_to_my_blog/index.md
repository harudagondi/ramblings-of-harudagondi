+++
title = "Moving My Site, and Why I'm Changing Static Site Generators."
date = 2020-12-01
[taxonomies]
tags = ["blog", "pelican", "python", "rust", "zola"]
categories = ["blog", "meta", "programming"]
[extra]
has_comments = true
has_hr = true
has_donate_button = true
+++

Hello guys! If you know my blog previously, you may have noticed that this is a completely different site, which is [harudagondi.gitlab.io][previous_blog]. Please read further if you wanna know why I moved sites.

## Pelican is hard to use

The first time I tried to run a blog, I experimented with [Jekyll](https://jekyllrb.com/), a static site generator that Github uses. Unfortunately, I could not set it up correctly, so I did not have a blog.

After Microsoft acquired Github, I then moved to Gitlab, where I found out that you could run a blog using [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/), where you could publish a static website for free, using a subdomain of gitlab.io (mine is at [harudagondi.gitlab.io][previous_blog]). I chose [Pelican](https://blog.getpelican.com/), because it is (1) written in Python, and (2) there's a lot of documentation written on the internet since it is one of the widely used SSGs (Static Site Generator) that is written in Python. It served me well for the past few years, but the more I tried to post more, the more frustration I feel.

Here are the reasons why:

### No hotloading

For me to see what the website looks like, I have to push the repository to Gitlab, then hope that the build is successful. You'll notice in the history that I put too many commits that only fixed the last commit (I'm a noob, lol). This is not ideal for the reasons stated in the next.

Also, according to the [documentation](https://docs.getpelican.com/en/latest/publish.html#site-generation), you can tell Pelican to auto-reload, which I don't even remember reading that in the past. Maybe I'm dumb.

### Inflexible

My problems with Pelican's themes are two-fold: one, each theme has different templates and; two, changing themes was hard, especially when you heavily modified the previous one. If you want to add plugins, you'd have to modify the templates themselves. Thus, when time passes and you want to change to another, more effort is needed to migrate.

This is a pain in the ass, so the original theme of the previous site was [alchemy](https://gitlab.com/harudagondi/harudagondi.gitlab.io/-/tree/master/theme/alchemy), a simple black & white theme. In the past, I wanted to change to another one. But since I have used so many plugins, using a new theme is infeasible.

### Plugins are hard to configure

Since version 3.0, Pelican supported [plugins](https://docs.getpelican.com/en/latest/plugins.html). Back when I was configuring the website, you would have to fork pelican-plugins to your own repository and put what plugins you want by listing them on the `pelicanconf.py` file. Now that I think about it...

### What's the difference between pelicanconf.py and publishconf.py

These two have confused me greatly. It seems that you can put any config variable in either of the two, and I remember that I was thoroughly confused by all the things you could do.

Additionally, there are some things I accidentally *did* with the publishconf.py, and I'm afraid to attempt to fix it. Here's a screenshot of my directory.

{{ image(alt="A screenshot of the harudagondi.gitlab.io project directory. Notice that there are seven extraneous copies of publishconf.py.", source="project_structure.png") }}

There are seven extraneous copies of publishconf.py, and apparently, there were committed as I mess around with plugins. Back then, I didn't know how to even revert git changes, so I let it stay there.

Therefore, my thoughts for Pelican would be:

**Conclusion**: Pelican is not for people who like to customize and also are noobs at programming.

## I also don't like Gitlab

Aside from the frustrations, I felt while tinkering with Pelican, there are some things I struggle with Gitlab. First, the interface is very confusing. When I want to change a setting in a Gitlab project, I'll have to scour through the millions of pages, pressing the dropdown button for each section just to find what I'm looking for.

Also, configuring `gitlab-ci.yml`, as a beginner to these things, is difficult. Consider that I have to browse through a lot of documentation, testing my hacks on *production*, and hoping that it will give a result, if not failing the pipeline. I have to memorize what arguments should I use so that I can run the website properly. It's hard, I know, but I wish there was something like a quick start guide or something. Working with Gitlab feels too tight.

On a more positive side, there are a lot of features; much more than what Github has.

## I'm moving to Rust and Zola

In recent years, I have been learning Rust to complement my skills in Python. I have dabbled in C code (We're tinkering with Arduinos for our school projects), and oh boy it is pain to use it! Using other people's code was a nightmare because sometimes these libraries have different instructions for using it for your own project. I'm glad that I have never touched any code regarding memory. I have never used any systems languages other than C and Basic; and I have read so many articles on the nightmares of having to manually manage the memory usage of your program.

[Rust](https://www.rust-lang.org/) touts itself as a programming language where you don't have to write your own memory allocator, whilst remaining fast, due to its programs being compiled rather than interpreted. *This is intriguing*, I thought to myself. *I don't have to think about complicated stuff?* So I started learning the language. I was getting frustrated with Python's limitations, anyway.

I'm glad I learned Rust. If you haven't noticed, I don't like being **frustrated**. When I write code and I get an error, I'll have to know whether or not it is part of my flow, or as some *gotcha* that I'll have to memorize for that programming language. Additionally, most languages don't even have helpful error messages, so I'll have to write a bunch of `print()` and `console.log()` all over the program just to get a sense of where the code went wrong.

On the other hand, Rust has very helpful error messages, making it easy to fix problems on the go. The debugger lets me focus on essential things, like logical errors, for example. It is no wonder the Stack Overflow users voted it as [the most loved language for four consecutive years](https://stackoverflow.blog/2020/01/20/what-is-rust-and-why-is-it-so-popular/).

Now that I dumped to you the reasons I like Rust, let's move on to Zola.

## What is *Zola*

Zola, like Pelican, is a static site generator (SSG). There are many reasons why SSGs are good, as presented in this [article](https://www.netlify.com/blog/2020/04/14/what-is-a-static-site-generator-and-3-ways-to-find-the-best-one/). Going back, Zola is written in Rust, which appeals to me a bit. After trying it out, I feel like this SSG is perfect for me. Here's why:

When running `zola init`, Zola will generate five empty directories and a `config.toml` file. That's it.

In my opinion, this is one of the best ways to learn how to make your own templates, and configuring them to your own needs. In Zola, there are no plugins, which sounds a bit odd since I use them on Pelican a lot. You have to make them yourself, which provides a lot of learning experience for me to grow as a programmer.

Currently, I have not used themes yet, as I have relied mostly on Bluma and some Sass code I've hacked together for this blog. Nevertheless, I think the main reason why I like Zola is that everything makes sense. I don't have to fight with the SSG; I only need to fight with myself.

I really like Zola, and I hope it can reach to version 1.0. Kudos to the developers!

Also, one thing I could do in Zola, that I found confusing in Pelican, is that I could group my writings to blogs and literature. That's a neat detail!

Also, I'm moving to Netlify. Just to let you know.

## What to expect from this blog

*So what?*

I intend to use this blog as a dumping ground for my thoughts. Expect to read my poorly thought posts about programming, social media, college, politics, and anything my hands have an itch on. I also plan to open commissions for my poetry and other literature *soon*, so look forward to that.

All in all, I hope I could keep this blog alive for the following years, as *finally*, I have the time and energy to express my thoughts on the interwebs.

I'll post my collection of poetry here, including the ones I post on Facebook and Twitter.

In the future, I'll add more features to this website, such as a comment system, and etc. Post your own thoughts below! I love to read them.

[previous_blog]: https://harudagondi.gitlab.io
