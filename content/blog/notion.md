+++
title = "Using Notion as the one-stop place for my thoughts, notes, and plans"
date = 2021-03-13
[taxonomies]
tags = ["productivity", "notion", "organization", "second brain"]
categories = ["personal", "productivity"]
[extra]
has_comments = true
has_hr = true
has_donate_button = true
+++

For the past few years, I have been scouring the world wide web for organization applications. Most of them I found lacking. In this post, I want to tell what are my thoughts on all the different apps that I have tried in the past.

## Evernote and other note-taking apps

My problem with it: I don't know what to write with it. Sure, I could take notes with it, but what kind of notes? These kinds of apps are best with short notes like grocery lists or what my friends like.

However, if I use it as an idea notebook, then it becomes a hassle. I am a writer, and I usually write my pieces in Google Docs. It seems that using both is finicky, and additionally, I don't think I can link Google documents and notes from EverNote together. The same goes with other note-taking apps I've tried.

My other gripe with these kinds of apps is that it is hierarchical. My mind is a mess, and I want my app to have the capacity to deal with it.

## TickTick and todo list apps

These apps are great for todo list apps. That's the problem. It's for only todo lists.

I don't want to download too many apps; that'll be too much for my brain. I rather have everything in one place.

## Tiddlywiki, Dokuwiki, and others

These kind of things are what I think is best for me. These kinds of apps can handle most of my thoughts, notes, and weird things in my mind. However, the biggest problem I have with these is that they are so hard to configure. Tiddlywiki in particular is too messy for me. Also, I find the number of themes and plugins lacking. Plus, it does not support backlinking by default; you have to download a fork of Tiddlywiki for that. It being a simple `html` file does not make it easier;

For Dokuwiki, the themes are nice, but running it is a nightmare. For starters, you would have to run an actual server, and since I don't have that kind of money, I just run it as a web server on my personal computer (which involves using a command line). Just using it is a hassle.

Both of these don't have Android/iOS support, and that's a deal breaker for me.

## Other apps

I also tried other apps, but they are too insignificant to say much about them. So here are the other apps!

- TreeWiki, Joplin: Too hierarchical.
- Obsidian: I haven't tried it, but according to its website, it looks like it's Tiddlywiki but with Markdown and backlinking. I would like to try it, but I'm put off by the cost of syncing (it's $4 per month if billed annually). I would like to try it if I have the capacity to earn my own money.
- Bunch of Markdown Files: No. It's a nightmare.

## Notion

So. [Notion](http://notion.so). What is it?

Imagine the apps above combined into one. That is what Notion is in a nutshell.

Personally, I've always wanted a feature-complete note-taking app (that is free). As a student with a messy mind, I would love to have a pseudowiki, complete with backlinking, support of any type of media, aesthetics, and the most important feature ever: databases.

What are databases, you ask? Well, they're like SQL tables in the sense that (a) it has columns and rows, (b) it can be filtered and sorted, (c) and it can connect with other databases using relations. Additional features include formulas (just like in Microsoft Excel), different views like List view, Calendar view, and etc.

It also features blocks, which is how you layout your own Notion pages. This gives Notion an advantage on aesthetics. I currently have an ultrawide monitor, and having the choice of making three columns is nice; it means that I don't have to see an ugly wall of text that span from left to right (this is why I usually make the styling of articles have the maximum width of around 600px — 700px). Notion also gives me the choice of various `html`-like blocks, such as headings (`h1`, `h2`, `h3`),  different kinds of lists, databases, media such as images, video, audio, and code snippets, and many more. I particularly like the Unsplash feature on the Image block, where you can get images from Unsplash itself without leaving Notion itself.

## What I don't like about Notion

There are some things I don't like about Notion. First, I don't actually own my data. This is a very bad sign, and I plan to move to Obsidian once I have a stable income in the future. Second, there's no offline support. Personally, I don't mind it, but if Notion is going to advertise it as the place where you'll store all your thoughts, then this is bad. I don't think this is reliable, and as such I'll still use Google Docs and other software in storing my current thoughts.

On to other less important stuff. I wish there were more font choices. I don't like the sans-serif font, and while I don't mind the serif font, I'd prefer IBM Plex's instead. The monospace font is okay. The media property in databases does not support getting it from Unsplash, so you have to add an image in the page itself when you want an image to show up in the gallery view. 

Additionally, there should be an automatic daily reminder feature. I would like this to act like a daily journal, but Notion does not support automating adding entries. Perhaps this is the Content API they are talking about?

There are also some minuscule things I like to see in Notion. The way of making new columns is not ideal. I wish I could adjust the width, and not have it in two fixed sized.

## Conclusion

I like Notion. But since it's a closed source software, I may have to move again to another one. I really think Notion's good, and I'll keep using it until I have a stable income so that I can consider paid options.

Also did you know that I wrote this post using Notion? Neat, huh?