+++
title = "Arguments for Mass Promotion (BatStateU Edition)"
date = 2020-05-09
[taxonomies]
tags = ["school", "mass-promotion", "rant", "arguments", "COVID-19"]
categories = ["rant", "school", "politics"]
[extra]
has_comments = true
has_hr = true
has_donate_button = true
+++

During the past few months, most of the Philippines has been placed on what is called the enhanced community quarantine, where the government placed restrictions on the movement of its citizens to limit the spread of COVID-19. This, of course, affected the daily lives of everyone negatively. Students are now forced to have online classes in the past few months to learn.

However, with the Philippines as a third-world country, and therefore not as developed as other nations, the repercussions of having online classes is massive on poor students. Today, I will argue why online classes, or as the BatStateU calls it, "online intervention," is not appropriate for the students of Batangas State University.

First, let me explain how this "online intervention" works. As of writing right now, the guidelines they have, have been changed. So in this blog post, I'll write about both guidelines and what I think of them, and what would be the implications and consequences, especially on poor students.

## The Old Guidelines

For reference, here are the detailed guidelines and instructions regarding the online intervention.

![The old guidelines.](guidelines.png)

Later on, I will discuss the contents of this guideline, especially number 2, titled *"Facilitation of Teaching and Learning"*.

## The New Guidelines

![The new guidelines](guidelines_new.png)

The new guidelines are just an amendment of the old guidelines, of which I'll summarize the important changes and add my opinions:

- *All remaining lectures and activities will be uploaded until May 9.*

This is good, actually, because the students won't have to be surprised because they were not informed.

- *There will be two simple activities (previously formatives) for each of the first two lesson topics. Drafting will have one to two plates only. Technology is also the same. MAPEH is one or **combined** activities for some components. For SHS, Research and Pananaliksik will require chapters 1-3. Math-related subjects require only one topic and activity. Writing and Speech require 1-2 activities.*

I think that converting formatives to requirements is a bad move, because students expected that formatives are not graded, therefore will be given much focus or attention.

Drafting, Research, and Pananaliksik are the biggest problems I have issues with. They require more than typical resources and gadgets to complete the activities. (Drawing plates and making in AutoCAD, researching tons of materials, and writing the document.)

- *The two graded activities should be accomplished on or before May 19 only.*

I was under the impression that this intervention (1) is self-paced, and (2) does not pressure you. Moving the deadline from the last week of May to the third week. Granted, this may seem like not a big deal, but it still adds more pressure, especially for people who don't have the means to comply.

- *When the combined scores of the activities are above 80 percent, then there will be an exemption in the quarterly exams.*

I was honestly surprised by this. At first glance, this seems like a good idea, however, there are problems. Learning management systems (LMS) are finicky when there are exams, especially when a student has an unstable internet connection. When a student taking an online exam had an interruption (electrical or internet-wise), then the exam will either be forfeited or automatically submitted, which is not ideal, especially when the teacher configured the exam to allow only one attempt, and if not, making the final score the average and not the maximum.

- *If you are not exempted from the quarterly exam, you'll take it from May 21-23.*

Isn't this a tad early? Originally, the final date of submission was usually from May 29 to 31. Why push it back?

- *Students who don't have internet access will be provided with hard copies once the quarantine is lifted.*

This is the weirdest one of all of them. First of all, what if the lift of quarantine is moved to June or July? We are not even sure when the government will lift it, especially the fact that [our curve is still rising](https://www.endcoronavirus.org/countries). I will focus on this later, including the earlier guidelines.

- *Contact your teachers if you have questions.*

No comment. [Some people on Twitter](https://twitter.com/abychingu/status/1257994554474561536?s=20 "Comment of a student on Twitter.") have commented on this, however.

Now that's out of the way, let me present my arguments why having online classes, or "online intervention," if you want to call it that way, is not ideal in our situation right. Note that this blog post is very BatStateU-centric, and some information may not apply to you, but I think that this useful in some regards.

# Reality

Let's consider the current situation right now.

We are in a **global pandemic**, where in Batangas, we are under the **enhanced community quarantine**, which means that the movement of the people is limited. This greatly affects the **livelihoods**, **available resources**, and **the normalcy**, that may take the toll on the **mental and physical capacity** of the people.

This situation is **exceptional**.

If you are a student whose family is still working, have enough money, and/or have the means of connecting to the internet, then guess what: you are very lucky. Students with more advantages in life are less likely to think about their survival in the near future. The opposite is true. While the privileged can dedicate their minds and time to their online classes, the lesser privileged possibly could not. They have to juggle their needs and survival with their education. With the current circumstances compounding the existing problems correlated with poverty, it is getting harder to catch up to students with more opportunities.

This pandemic does not make us *equal*; it **amplifies the gap** between low and middle-class students.

Merely because of this fact, the problem cascades with aspects of online education, further worsen the quality of education the disadvantaged student should be receiving. In the following paragraphs, I will explain how small disadvantages can intensify to greater differences between the poor and rich students.

# Studying in a Good Environment

Every place has an associated "mood" or "context" with it. Malls have the "entertainment" feel, work offices have the "professional" vibe, schools are dedicated to "learning," and homes are where all the personal and familial stuff happens. It is expected that when you are at the office, you are expected to work. At the mall, you are expected to shop, eat, be entertained, or even just wander. The point is, each place expects your action, and your brain adjusts to the current situation. The appropriate place will likely to elicit appropriate thoughts and actions.

That's why many articles suggest you have a dedicated study room or place (such as a library) because it's easier not to mix up home and school-related activities (this also applies to work-from-home arrangements, where it is recommended to dedicate a room for work-related stuff; to get you onto the 'groove').

But when a big negative event happens in your life (such as death from your family, loss of the means of income, etc.), then the context, which should be in one place i.e. your home, will bleed through other contexts, which can negatively affect your performance in other situations. A typical response from a school would be to excuse the student for them to be able to cope with their problems.

Mixing up home and school activities are bad. Imagine the time when our situation right now is normal. You wake up, eat and bathe, ride a jeepney, learn at school, ride a jeepney again, and do your chores. The commute is an excellent way to transition your mental state from "home" to "school". However, if we remove that from our equation, then the lines between home and school are getting blurry. Sure, with enough discipline, you can achieve it. But that requires dedication and time, and one month is not enough (Some may say two and a half, but BatStateU stated that the start of the online intervention is in May, so I'm going with one).

Additionally, if you are poor, then it's most likely that you would help your family provide income for you and your family's survival. Your mind will be clouded in various thoughts like "what can you do to help", "what will I eat for tomorrow", "where will I get the food," etc. Having an online curriculum burden you more, thus hindering your mental capacity to effectively comply with activities and tasks. Remember, the pandemic also exists, so add that to your list of problems.

Specific to Batangas, many rural areas do not have strong signal strength. This forces students to make a choice: whether to risk you and your family's health by going outside for stronger signal strength or to just resign and force yourself through the poor (or sometimes non-existent) internet connection. Also, some families do not have a Wi-Fi router at home (usually because no internet provider covers their area), thus using a data connection, which is more expensive, notably when you need to watch videos or download large files.

Also, consider this: if you decided to risk your health by going to the mountains or other places where signal strength is stronger, then another problem has appeared! Remember earlier, when I said that the "appropriate place will likely elicit appropriate thoughts and actions"? Well, let me ask you this: is the mountains a conducive environment for you to study, learn, and take exams and activities? For example, in a [tweet made by a teacher](https://twitter.com/SirEmPhi_/status/1258237721983594496?s=20 "a picture of a person studying outside"), is the environment conducive for learning, where they are basking in the hot rays of the sun? I don't think so. The poor and disadvantaged people are forced to make bad choices, that impairs their ability to be academically acceptable.

Lastly, BatStateU surveyed all of its campuses and found that 75% of the students have unstable internet that restricts them from having online classes. It is the majority of the students that will be greatly affected by this, and not the minority.

![Survey of all the students](survey.jpg)

# Lack of Resources

This topic overlaps with the last one regarding the expenses of internet connection. Also, I'll discuss the problems with the new guidelines regarding the simple activities and copy-paste the snippet from earlier (emphasis mine).

> - *There will be two simple activities (previously formatives) for each of the first two lesson topics. Drafting will have one to two plates only. Technology is also the same. MAPEH is one or **combined** activities for some components. For SHS, Research and Pananaliksik will require chapters 1-3. Math-related subjects require only one topic and activity. Writing and Speech require 1-2 activities.*
>
> I think that converting formatives to requirements is a bad move, because students expected that formatives are not graded, therefore will be given much focus or attention.
>
> **Drafting, Research, and Pananaliksik are the biggest problems I have issues with. They require more than typical resources and gadgets to complete the activities. (Drawing plates and making in AutoCAD, researching tons of materials, and writing the document.**)

Not all students have a desktop computer. Some have a laptop, and a few only have a phone. Normally, this can be alleviated by going to a computer shop or the school's computer laboratory, but **due to the pandemic** these options are not viable. Also, since some barangays require a *quarantine pass* then it is harder for students to even go to their *neighbors*[\*](#_ "if your barangay is strict.").

Let me list down some system requirements for AutoCAD 2015:

- OS: at least Windows 7
- Memory: minimum of 2GB (8GB recommended)
- Disk Space: installation 6GB

A friend I personally know has a sibling who needs to use AutoCAD. Their laptop has 2GB memory and 32GB storage space. However, Drafting is not the only subject that requires the use of the laptop. There is also programming, graphics and video editing, and multiple documents that cramp up the laptop's storage space. Also, that disk space requirement above is just the fresh install! Files made by AutoCAD are large enough that it can quickly fill up storage.

Of course, this was explained to the teacher, and so they were given a different task to do. As for my opinion, isn't this unfair? You are measuring each student differently, instead of applying the same activities to each student no matter what. It's a lose-lose situation for the teacher because they are either letting the student fail because of circumstances they cannot control, or be unfair to the majority of students because you are measuring them by different standards.

Another problem is research because projects of the subjects are usually submitted by groups. What if one of your groupmate could not participate in the project because they have a poor internet connection? That's one less workforce and more workload for others. If the teacher makes you evaluate your groupmates, then do you grade that person lower because they did not participate, or higher, which is actually academically dishonest?

Also, tools such as Google Docs, are data intensive. The alternative is to send files to each other, which is not ideal and can cause confusion. Moreover, Google Docs have lesser features on the phone than on the desktop, which does not help people who only have phones in the slightest.

# The Life of the Students does not Revolve around the School

In continuation from *"The Reality"* and *"Studying in a Good Environment"*, the students do not only face problems in online education but also in their day-to-day activities.

For example, my mom has to go out and to another barangay to take care of our piggery farm. My uncle works in a feed mill, which is related to agriculture, so he is required to report to work every day. When we sell our pigs, the hauler will pick them up; they usually come from and to Laguna or Cavite.

*There is a real risk for me and my family to become infected from COVID-19.*

There are persons under investigation in our neighboring barangays, and my neighborhood is in quarantine because someone came home from Laguna. *This is stressful*, and that stress negatively affects my mental capacity to comply with my requirements, thus possibly worsen my grades.

In fact, one of my friends had [a family member that died just recently](https://twitter.com/heinzpua/status/1257933168406978560?s=20 "My friend's uncle died, possibly from COVID-19."). In response, his school just extended its deadline to June 5.

This isn't an isolated case. Because of the ubiquity of the situation, everyone is at real risk of dying. If all have the risk, why not lessen the risk by lessening the burden? Stress can impair your immune system, which puts us more at risk of infection. By removing the burden to pass the requirements, then there is a lesser risk of impairing the immune system, thus lessening the risk of dying.

# Band-aid Solutions

All solutions presented by BatStateU-IS are *not enough*.

Let's look at the old guidelines in detail first, because it is more comprehensive than the newer one (emphasis mine):

> 3. Faculty members shall facilitate the teaching and learning process through any alternative / flexible delivery mode such as but not limited to teaching / consultation through any available online platform, email, social media accounts, **mobile phones (SMS)**, **landlines**, **ham radios** or **strategic use of printed materials** as may be agreed upon by the students and faculty and approved by the College Dean as recommended by the course syllabi... 5. If after exhaustion of all means, any electronic medium is still not feasible, **the hard copy of the lectures or other teaching materials as well as examination / final requirements with detailed instructions to students can be dropped off by the faculty members at the nearest campus guardhouse for pick up by the students concerned or his representative.**

Let's address the bolded text.

For SMS, how can you take a test with it? Considering the number of lectures, most in video format, it is not feasible to send them through text only.

For landlines, the problem is the same: it is very hard to learn from audio only. Also, how can you take a test on the landline? What if the student had hearing problems? Does the caller have enough patience? What if you want to revisit your lessons? Some telephones don't have support for recording audio.

For ham radios, um, who have heard of ham radios? These kinds of radios need a license.

The hard copies are the biggest issue. First, not all students of BatStateU live in Batangas City. Some live in Mabini, Bauan, and Lobo, which are in different towns. Due to the enhanced community quarantine, it is not legal to travel to other towns. This conflicts with people who live far away. Notably, the concern is raised by [one person featured on the Lathe](https://twitter.com/TheLATHEPub/status/1258604973954232321?s=20), the student publication of Batangas State University.

# Why Mass Promotion

The goal of the online classes should be for the students to learn. However, as stated above, multiple reasons diminish the effectiveness of online classes, thus rendering them not as useful for underprivileged students. Mass promotion is a solution to lessen the burden of these students so that they can redirect their energy on their needs while being fair to the other students by holding them all to the same standards by treating their grades the same.

Nonetheless, this does not mean that all the efforts of the teachers are wasted. The students and the teachers can coordinate with each other whether or not they want to continue the online lectures. Mass promotion only affects the activities and not the lessons, because a goal of the mass promotion is to lessen the pressure and burden of both sides by lessening the workload of both the students (*by not doing the activities*) and teachers (*by not making and checking the activities*).

The goal of the school is for the students to learn and the teachers to teach. Testing is just a tool to measure how effective is the pedagogy of the teachers and the intellect of the students, and therefore secondary. Thus, the efforts of the teachers will not be wasted and the students may have the chance to learn the academic materials more effectively because they are not pressured to pass the requirements. In fact, because the teacher does not need to relay the grades of the students to the principal or a higher authority, then they, too, are more likely to not be pressured to pass their own requirements.

It is a win-win situation, and the burden of both parties is now reduced.

## Common Rebuttals I See

> Isn't this unfair to smart people?

Possibly. But let me ask you this: isn't the current situation unfair to people who are smart but have no means to pass the requirements?

> Isn't this unfair to the teachers who made all the lectures?

Mass promotion does not mean that there will be no lectures. Rather, the lectures will be optional, and I think that most students will likely opt-in, so that future lessons will be easier to understand.

> This is already our last month of the academic year. Why not continue it?

This is the [escalation of commitment or the sunk-cost fallacy](https://en.wikipedia.org/wiki/Escalation_of_commitment "Wikipedia: Escalation of Commitment"). This month being the last month does not mean we should continue the academic year. Investing nine months in the academic year does not mean we need to invest another one.

> Why not tell your concerns to the appropriate authorities?

I don't know about your situation, but [read this to understand ours](https://twitter.com/abychingu/status/1257994554474561536?s=20 "Ranting on Twitter.").

# Thoughts?

Please hit me up on Twitter if you have any questions. Dogpiling and insulting me will not be tolerated; you will be blocked.