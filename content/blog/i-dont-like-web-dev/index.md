+++
title = "I don't like web development"
date = 2021-08-08
[taxonomies]
tags = ["blog", "web development", "html+css+js"]
categories = ["blog", "programming"]
[extra]
has_comments = true
has_hr = true
has_donate_button = true
+++

Yeah. I do not like it. Here's why.

## I hate the current state of the internet

The internet is place where you can deploy your own site for the world to see. Although the current state of the web is atrocious{% margin_note() %}Facebook being one example.{% end %}, I still believe that we can do something about it. However, this isn't my current gripe with the net right now.

It's how bloated it is.

Consider my situation. I live in the Philippines. According to this [Vice Article](https://www.vice.com/en/article/n7vy3m/why-internet-speeds-philippines-slow-laws), the internet speed here averages out to around 32.37 megabits per second. However, this is *not* representative of most Filipino experience. As of writing, my internet speed is 1.8 Mbps. If my internet provider is being generous, I could sometimes experience at least 10 Mbps. But it is more than usual that I experience lower speeds than that. Sometimes, I get 100 Kbps on certain days, and that could go lower!

{{ image(alt="My internet speed of 1.8 Mbps.", source="internet_speed.png") }}

Because of that, most basic social media are unusable on certain days. Using data is possible, but impractical. Fifteen minutes of using data for browsing Twitter consumes around 30-50 MBs, which is one seventh to one fourth of my current data plan. 

As such, here is my philosophy on how the internet should work:

## My philosophy on the world wide web.

A website is usually composed of two things: the front-end and the back-end. Let's discuss the front end first.

The front end is made up of HTML, CSS, and Javascript files. In my philosophy, here is what a website's front end should be made up of:

### Content

The content of a website is whatever you see, read, hear, watch, and etc. It is what gives your site the meat for your audience to chew onto. This is the most important of your website, and if your content is hard to understand, then don't expect most people to read it.

### HTML

If the content is the meat of the site, then the HTML is the skeleton. HTML guides the organization of your content; how should it be read or displayed to the users without any frills or decorations. It also describes what is the type of your content, whether it is an image, article, sections, forms, videos, or others, through the use of various semantic HTML tags.

### CSS

The CSS handles how should the lump of bones and meat look like. Using the skeletal structure we defined earlier, we describe the actual visual of your website. It shows the colors, margins, paddings, and other things that should be for aesthetics only.

### Javascript

Hoo boy.

I honestly think that most websites misuse Javascript, and as such, is one of the reasons why the web is so bloated.

Here are the only reasons why should you use Javascript:

1. The content should be dynamic; or
2. The content should be interactive.

That's it.

*Wait, what do you mean that the content should be dynamic or interactive?*

Good question!

Dynamic content is content where it changes with or without visible input from the user. An example of this is a clock: it's dynamic, but it does not necessarily need active interaction from the user, only needing the access to the user's time locale. Another example is Reddit's *how many users are online* counter on the sidebar. It is dynamic: it only changes when the amount of online users changes.

Interactive content is content where it requires active interaction from the user. This isn't a POST request{% margin_note() %}The content of the server changes, but not necessarily the content of the frontend.{% end %}. The nearest example is most of Nicky Case's [works](https://ncase.me/), and their other site, [Explorable Explanations](https://explorabl.es/). This is where dynamic and interactive content intertwine, but I would define in Javascript terms as something where you need to get user events, such as `onclick` and family.

Those things are the only things where Javascript should be used. No, tracking users isn't making the content dynamic nor interactive. Why does CSS-in-JS exist? CSS should not absolutely be dynamic nor interactive.

## Here is what I hate in HTML+CSS+JS

### Unclear HTML Semantic Tags

The two most useful HTML tags are `<div>` and `<span>`. However, since bots are stupid, they could not tell what the contents inside those tags are. As such, web crawlers could not understand what your website is all about, and it could seriously impact your SEO or something idk what those words actually mean. 

Enter semantic tags.

Semantic tags help identify which content is the most important and which isn't. It is how you tell a Google bot the difference between a navigation bar, a footer, and an article.

There are some things unclear with these tags.

First, the `<article>` and `<section>`. Is an article composed of many sections, or a section is composed of many articles? It is unclear. Since W3Schools sucks, I am currently browsing the MDN docs.

Apparently `<section>` is so generic it can be used as something like an article or a navigation bar? But isn't the point of semantic tags is to determine whether something is more important than the other, like how article > navigation bar?

Also what the hell is this?

{{ image(alt="Making your element hidden from screenreaders is apparently positioning it with a magical number?", source="screenreader-hack.png") }}

### CSS on its own sucks

When I first started using CSS, it was a godsend. No more inlining styles. Just define the aesthetic in a separate file. I really like the separation of content from style.

Anyways CSS sucks because I'm stupid and the box-sizing is very unintuitive to me. But that's not the only gripe I have.

You see, I'm not experienced enough to make my website responsive to different screen sizes. Also why is making margin notes so hard! I am very envious of other blogs having fancy things like margin notes, dialogue (like in fasterthanli.me), and admonitions. I really like those, and I wish there was a CSS framework for that. Speaking of...

### Why are there so many CSS frameworks

Why.

Originally, there was Bootstrap, and it made sense to use it on sites which are very maximalist.

And then I think everyone hated the grid system{% margin_note() %}The frameworks, not the CSS grid.{% end %}, and so everyone moved to flexbox. Also a lot of frameworks spawned for the past several years and they all look slightly different.{% margin_note() %}The visuals are just some variation of Google's Material Design, which is actually very good!{% end %}


Also there are a lot of 'CSS' frameworks that are bundling with JS. Why? The goal of CSS is to make your content stylish, not make it dynamic nor interactive. Why bundle it with Javascript?

### I don't like building things/I hate the Javascript language/I don't like node in general

Once upon a time people write CSS and Javascript until they hated it because it does not absolutely scale at all for big websites. Even small websites are a nightmare to style if you have any resemblance of a color scheme. You may say that modern CSS has variables now, but the syntax is very unwieldly (and ugly! personally). So people wrote Less and Sass and Coffeescript and Elm and React and Vue and whatever the hell webdev people come up with every six weeks. The problem with those is obviously browser can't understand them. So people work around this by compiling these to pure CSS or Javascript. The thing that I don't understand is that why write the compiler in *Javascript*. WTF.

Javascript is not my favorite language. It claims to be a functional and prototypical language despite the fact that it's standard library does not have anything related to iterators until the introduction of EcmaScript 6 (Why is it called EcmaScript?). Also if Javascript is an object oriented language then why does it have weak typing? I know that the core of OOP is [bundled state and behavior](https://eev.ee/blog/2013/03/03/the-controller-pattern-is-awful-and-other-oo-heresy/), but I like my objects to be a specific bundle of state and behavior unless I explicitly converted that object to another type.{% margin_note() %}This is why TypeScript exist.{% end %}

ES6 solved most of Javascript problems. It finally have iterators! Also the only valid for loop to use is `for...of` and all variable declarations should be `let` instead. I still don't understand why `var` is implemented... like that.

Using other Javascript code is on another level. Originally there is only one Javascript Interpreter: the browser (technically the number of interpreters is the number of browser engines available on the internet). Javascript is just a neat little programming language to make your content more dynamic. And then someone thought it was a good idea to separate the browser engine out of the browser, added some new frills such as namespacing and called it Node.js. 

This led to my confusion back then. I did not understand that to basically import something on the web, you just add a `script` tag and add the link to the CDN, or a file you downloaded earlier. The implications of this is that the more libraries you use, the more bloated your website is. Even helper libraries such as jQuery and Lodash add to the bandwidth usage of your users.

The solution? Apparently most developers' answer is to just don't give a shit.

I'm guilty of this! As you notice, this static website uses jQuery to basically implement the navigation bar burger button and the image modals{% margin_note(inline=true) %}Which basically does not work right now. :cry:{% end %}. I used jQuery because honestly, raw Javascript is just ugly, and jQuery is more familiar to me. However, since modern browsers are now less annoying with their nonstandard practicees, jQuery get more and more useless.{% margin_note() %}As of this writing, I replaced jQuery with [UmbrellaJS](https://umbrellajs.com/), which is only less than 10% of jQuery's file size.{% end %}

I was also saddened by the fact that `import` does not work in browser Javascript (well they do right now). Back then, I thought that javascript can now be namespaced so I can know which functions came from which dozens of libraries I added for my site.

I also wish simply importing scripts does not run its side effects. In Python, you wrap your main code with `if __name__ == "__main__"` so that your code wouldn't run arbitrary code. Rust requires an explicit main function so side effects basically does not exist.

Anyways, let's move on to Node!

What is node?

Node is basically if you take the Chrome's V8 Javascript engine and interface it with IO. You can now access the filesystem, print to stdout, and do some HTTP stuff. I do not know why would you want Javascript to interface with the outside world especially when it is prone to type errors and most times it is silent unless you `"use strict";`.

Node's package manager is called Node Package Manager{% margin_note() %}Duh. 🙄{% end %} or npm for short. Before I used node, I have already heard of npm horror stories, like [left-pad](https://www.theregister.com/2016/03/23/npm_left_pad_chaos/), or [- (the package)](https://www.bleepingcomputer.com/news/software/empty-npm-package-has-over-700-000-downloads-heres-why/) or even [npm audit](https://www.bleepingcomputer.com/news/software/empty-npm-package-has-over-700-000-downloads-heres-why/).

My recent dealings with npm does not make it any better. While working on this blog, I tried to use [TailwindCSS](https://tailwindcss.com/). I think the CSS framework is really neat, especially the functionality of purging unused CSS. But to have that feature, I'll have to use npm.

Currently I actually use yarn, because before writing this blog post I actually want to write a blog about using [Tauri](https://tauri.studio/) to make an asynchronous download manager GUI using Svelte and Rust. Tauri suggested that I use yarn because it works on their machine (yeah).

So coming back to this blog, I found this tutorial on integrating [TailwindCSS and Zola](https://www.maybevain.com/writing/using-tailwind-css-with-zola-static-site-generator/). According to this, I should do the following:

1. Do `npm init` or anything equivalent.
2. Install `tailwindcss` (not tailwind!), `postcss`, and `autoprefixer`.
3. Also install `parcel-bundler` to have yourself a build system.
4. Create a `.css` file (which is actually PostCSS), and add a bunch of TailwindCSS boilerplate.
5. Build that with `parcel watch` or `parcel build`, to make it work.

Here are a couple problems I've discovered:

#### Doing that does not work

When I finish the tutorial, I got my setup working. It builds! However the CSS *does not work*. The `styles/styles.css` file looks like this:

```css
@tailwind base;
@tailwind components;
@tailwind utilities;
```

Its output looks like this:

```css
@tailwind base;@tailwind components;@tailwind utilities;
```

Okay?

Admittedly this is the first time I have heard of PostCSS. Is this just like Sass and Less? One thing I like about Zola is that it has builtin Sass compiler, since there is already a [crate](https://crates.io/crates/sass-rs) for that. PostCSS, unfortunately, does not, and so I would have to use Node.

Second, yarn keeps throwing errors at me? Something about types? I am not using TypeScript. Why would an error clearly about a package complaining that I didn't define my types? Also, I have two config files: one for TailwindCSS and one for PostCSS, which are both written in Javascript? What? Also yarn keeps telling me that TailwindCSS needs PostCSS 8, despite the fact that I already installed version 8?

All of this is very confusing, and I don't know where to start. TailwindCSS docs expect me to use some Javascript Component-based library or PostCSS, and PostCSS is giving me so many problems! Additionally, PostCSS's tagline is "A tool for transforming CSS with JavaScript" which goes against my philosophy. Apparently PostCSS just serves to fix the most annoying CSS issues such as a global namespace and stupid amounts of vendor prefixes. But that shouldn't be handled by the developers, in my opinion. That should be handled by the browser themselves! The W3C exists, but due to the current landscape of browser competition, standards are mostly useless.

I think it's because of people who are still using Internet Explorer. Why else would quirks mode [exist](https://eev.ee/blog/2020/02/01/old-css-new-css/#quirks-mode)?

On the other hand, why does my `node_modules` folder have 580 folders inside? What the hell? Does installing these four packages requires that much dependencies? As it turns out, Javascript people really love micropackages, where each package is just a few lines long. Remember `left-pad` from earlier? Originally, it is only 11 lines long, written exactly like this:

```ts,linenos
module.exports = leftpad;
function leftpad (str, len, ch) {
  str = String(str);
  var i = -1;
  if (!ch && ch !== 0) ch = ' ';
  len = len - str.length;
  while (++i < len) {
    str = ch + str;
  }
  return str;
}
```

According to The Register, left-pad was fetched 2,486,696 times for the last month of that time. That is insane.

Personally, I don't have a problem with micropackages. The real problem here in my opinion is Javascript's lack of standard library. It claims to be a functional language, yet something like lodash is a separate thing! Handling big numbers is a nightmare since JS has no concept of an integer, and iterators does not exist until the last big update, despite the fact that a lot of JS code relies on functional paradigm. Why is Javascript like this.


 In its core, Javascript is simply just a language designed to make the internet more dynamic and interactive. Therefore, the most features available to you at the time was manipulating the DOM. However, people discovered that Javascript could do more than it was intended to do, and thus people started abusing it to hell and back.

 Then a lot of browsers started popping up and inconsistencies between Javascript engines started to increase. Eventually jQuery was made to polyfill older or outdated browser versions and keep up with the latest version. Then various helper libraries appeared, from Lodash to React. 

 The internet is bloated. And it is probably all Javascript and browsers' fault.

 ## So how do we fix it?

 Hmm. I don't know. I am definitely not qualified enough to answer that question since I do not know the intricacies of web development. Despite that, let's try answering that question!

### Use less Javascript

Please do not use Javascript to stimulate something that can be done in HTML or CSS. What do I mean by this? Here are some examples:

* Making a link by using `onclick` on a `div`
* Stimulating a textbox{% margin_note() %}Twitter does this. Try inspecting any textbox. It isn't an `input`. It's a `div`.{% end %}
* Stimulating a `<select>`
* Adding an `hr` after every heading (you have `:after` for this)
* And many other things

Try going on a day of websurfing with Javascript turned off. You'll be surprised how much reliant a lot of websites on Javascript.

HTML5 exists. CSS3 exists. Use it. Please.

### Try using Wasm

If you rely on using Javascript heavily (for example: 3D stuff, single page applications, etc.), then you could benefit from [Web Assembly](https://webassembly.org/), or Wasm for short. [File size wise](https://lemire.me/blog/2018/10/23/is-webassembly-faster-than-javascript/), JS wins on smaller code, but Wasm scales better on larger programs. 

### Use alternative protocols

One neat thing I discovered in this pandemic are the [Gemini Protocol](https://gemini.circumlunar.space/) and the [Hypercore Protocol](https://hypercore-protocol.org/). These offer a lightweight alternative to the HTTP protocol. 

With the Gemini protocol, you are definitely restricted with what content you offer to the web. There are no inline links; they must be represented as list of links. Gemini has only three levels of headings, unordered lists, and block quotes, and that's it. It has no CSS, and thus the styling is up to the reader. It has no Javascript, which adds the benefit of privacy and lightweightedness.

Sure it is restrictive, but isn't the essence of static websites its simplicity?

If you want your website be dynamic but lightweight, try the Hypercore protocol. It is basically the same as the HTTP protocol, but operates on a **peer to peer** basis. This opens up some new restrictions. First, your website should be able to be distributed with the limitations of your own bandwitdh. The website is has a lot of jargon for me to comprehend, but I think that it's a neat idea:smile:.

## Anyways we are VERY offtrack with our topic.

Right. What's the title of the post again?

*"I don't like web development"*?

Yeah.

Most people would tell you that Javascript is bad because of implicit conversions or whatever. I don't *like* it either, but that the least of my concerns.

I don't like Javascript beyond the Javascript. I don't like the standard library (or lack thereof). I don't like the confusing documentation (which one should I use? W3Schools? Or MDN Docs?). I don't like the ecosystem of HTML+CSS+JS right now. I don't like how because of the quirks of past browsers, we are stuck with what we have today. I don't like how unintuitive and unwieldly CSS is for me, and how modern CSS frameworks rely on Javascript to just *work*. I don't like how solutions to hard problems like scaling your website is to just throw more Javascript to it instead of, I don't know, create a more sensible language and compile to Javascript instead{% margin_note() %}[Mint lang](https://www.mint-lang.com/) looks so good, except for the fact that documentation is basically non-existent. If I waited just a bit more, this probably would work as a ReactJS/Vue/Elm alternative.{% end %}?

What if we had a language that could compile to HTML+CSS+JS and not worry about stupid node.js and npm and whatever shenanigans browsers keep implement that would make each website slightly different on different browsers? Something like Svelte but less like HTML+Javascript? Like, imagine Wasm but instead of an alternative for Javascript, it's the alternative for HTML+CSS+JS and we could do whatever we want here. We don't even have to do something like a component based system!

I don't really know what I am talking about here.

I wish web development is more accessible as a noob to this. I don't want to know every tiny detail about attributes on every HTML tag. I don't want to learn a lot of CSS stuff like flexbox and grid-css and what new and hot CSS framework people come up with. I don't want know which JS framework should I use from the hundred of thousands of packages found on npm.

What if I was not actually so stupid for comprehending such simple things. HTML to structure your content. CSS to decorate your content. JS to make your content dynamic and interactive.

Anyways.

### Things I like about the web

Despite the weird things happening in the internet landscape, I think there are some things I like about web development!

First, HTML+CSS is one of the most robust toolkit for designing user interface. It is the appeal to tools like Electron and Tauri for building cross-platform GUI applications.

Wait, that's not about web development.

Elm has a really good idea on architecture, however I wish it isn't so *functional*. Same with Mint. Javascript is not just a functional language, but it is also an OOP. 

Kotlin looks cute! I have not tried it outside of writing Android applications{% margin_note() %}Writing Android applications is a nightmare, btw. The documentation is *complete*, but it is so unreadable to me. Youtube videos does not help.{% end %}. With its strong typing, it is one of the most feasible programming language as a replacement to Javascript. Brython comes a close second, except you have to include a separate Javascript library to make your Python code run. I want a compiler, not an interpreter. I think that your webpages should be renderable even if your Javascript code fails.

With the advent of Web Assembly, any compiled language could target Wasm and we could stop all of these nonsense. I wish my package manager was like Cargo but as rich as npm. I wish I had the power of something like Haskell and Scala and Python and  other nice things Javascript could have been. Any language that doesn't have the same thing as `this` in Javascript and I would've been fine with it!

But we are stuck with Javascript. Forever. :ghost:

The end.

/rant